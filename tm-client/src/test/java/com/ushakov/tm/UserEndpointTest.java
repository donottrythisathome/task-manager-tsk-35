package com.ushakov.tm;

import com.ushakov.tm.endpoint.*;
import com.ushakov.tm.marker.SoapCategory;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;

public class UserEndpointTest {

    @NotNull
    final AdminUserEndpointService adminUserEndpointService = new AdminUserEndpointService();

    @NotNull
    final AdminUserEndpoint adminUserEndpoint = adminUserEndpointService.getAdminUserEndpointPort();

    @NotNull
    private SessionEndpoint sessionEndpoint;

    @NotNull
    private UserEndpoint userEndpoint;

    @NotNull
    final private String userLogin = "admin";

    @NotNull
    private Session session;

    @Before
    @SneakyThrows
    public void before() {
        @NotNull final SessionEndpointService sessionEndpointService = new SessionEndpointService();
        sessionEndpoint = sessionEndpointService.getSessionEndpointPort();
        @NotNull final UserEndpointService userEndpointService = new UserEndpointService();
        userEndpoint = userEndpointService.getUserEndpointPort();
        session = sessionEndpoint.openSession(userLogin, "admin");
    }

    @Test
    @SneakyThrows
    @Category(SoapCategory.class)
    public void setPasswordUser() {
        @NotNull final String oldPassword = adminUserEndpoint.findUserByLogin(session, "user").getPasswordHash();
        userEndpoint.updateUserPassword(session, "newPassword");
        @NotNull final String newPassword = adminUserEndpoint.findUserByLogin(session, "user").getPasswordHash();
        Assert.assertNotEquals(oldPassword, newPassword);
    }

    @After
    @SneakyThrows
    public void after() {
        Assert.assertFalse(sessionEndpoint.closeSession(session));
        @NotNull final Session tempSession = sessionEndpoint.openSession("admin", "admin");
        adminUserEndpoint.removeUserByLogin(tempSession, userLogin);
        Assert.assertFalse(sessionEndpoint.closeSession(tempSession));
    }

}
