package com.ushakov.tm;

import com.ushakov.tm.endpoint.Session;
import com.ushakov.tm.endpoint.SessionEndpoint;
import com.ushakov.tm.endpoint.SessionEndpointService;
import com.ushakov.tm.exception.AbstractException;
import com.ushakov.tm.marker.SoapCategory;
import org.jetbrains.annotations.NotNull;
import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;

public class SessionEndpointTest {

    @NotNull
    private final SessionEndpoint sessionEndpoint;

    public SessionEndpointTest() {
        @NotNull final SessionEndpointService sessionEndpointService = new SessionEndpointService();
        sessionEndpoint = sessionEndpointService.getSessionEndpointPort();
    }

    @Test
    @Category(SoapCategory.class)
    public void openCloseSession() throws AbstractException {
        @NotNull final Session sessionTemp = sessionEndpoint.openSession("test", "test");
        Assert.assertNotNull(sessionTemp);
        Assert.assertFalse(sessionEndpoint.closeSession(sessionTemp));
    }

}
