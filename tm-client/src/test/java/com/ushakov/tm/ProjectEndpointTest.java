package com.ushakov.tm;

import com.ushakov.tm.endpoint.*;
import com.ushakov.tm.exception.AbstractException;
import com.ushakov.tm.marker.SoapCategory;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;

public class ProjectEndpointTest {

    @NotNull
    private ProjectEndpoint projectEndpoint;

    @NotNull
    private SessionEndpoint sessionEndpoint;

    @NotNull
    private final String projectName = "projectName";

    @NotNull
    private final String projectDescription = "projectDescription";

    @Nullable
    private String projectId;

    @NotNull
    private Project project;

    @NotNull
    private Session session;

    @Before
    public void initializeTest() throws AbstractException {
        @NotNull final ProjectEndpointService projectEndpointService = new ProjectEndpointService();
        projectEndpoint = projectEndpointService.getProjectEndpointPort();
        @NotNull final SessionEndpointService sessionEndpointService = new SessionEndpointService();
        sessionEndpoint = sessionEndpointService.getSessionEndpointPort();
        session = sessionEndpoint.openSession("user", "user");
        projectEndpoint.createProject(session, projectName, projectDescription);
        projectId = projectEndpoint.findProjectByName(session, projectName).getId();
    }

    @Test
    @Category(SoapCategory.class)
    public void findAllProject() throws AbstractException {
        Assert.assertEquals(1, projectEndpoint.findAllProjects(session).size());
    }

    @Test
    @Category(SoapCategory.class)
    public void findProject() throws AbstractException {
        Assert.assertNotNull(projectId);
        Assert.assertNotNull(projectEndpoint.findProjectById(session, projectId));
        Assert.assertNotNull(projectName);
        Assert.assertNotNull(projectEndpoint.findProjectByName(session, projectName));
        Assert.assertNotNull(projectEndpoint.findProjectByIndex(session, 0));
    }

    @Test
    @Category(SoapCategory.class)
    public void removeByIdProject() throws AbstractException {
        Assert.assertNotNull(projectId);
        projectEndpoint.removeProjectById(session, projectId);
        Assert.assertFalse(
                projectEndpoint
                        .findAllProjects(session)
                        .stream()
                        .anyMatch(p -> p.getId().equals(projectId))
        );
        projectId = null;
    }

    @Test
    @Category(SoapCategory.class)
    public void createProject() throws AbstractException {
        projectEndpoint.createProject(session, "test", "test");
        @NotNull final String tempProjectId =
                projectEndpoint.findProjectByName(session, "test").getId();
        Assert.assertNotNull(tempProjectId);
        Assert.assertNotNull(projectEndpoint.findProjectById(session, tempProjectId));
        projectEndpoint.removeProjectById(session, tempProjectId);
    }

    @Test
    @Category(SoapCategory.class)
    public void updateByIdProject() throws AbstractException {
        @NotNull final String newProjectName = "newProjectName";
        @NotNull final String newProjectDescription = "newProjectName";
        projectEndpoint.updateProjectById(session, projectId, newProjectName, newProjectDescription);
        @NotNull final Project updatedProject = projectEndpoint.findProjectById(session, projectId);
        Assert.assertEquals(projectId, updatedProject.getId());
        Assert.assertNotEquals(projectName, updatedProject.getName());
        Assert.assertNotEquals(projectDescription, updatedProject.getDescription());
        Assert.assertEquals(newProjectName, updatedProject.getName());
        Assert.assertEquals(newProjectDescription, updatedProject.getDescription());
    }

    @Test
    @Category(SoapCategory.class)
    public void startByIdProject() throws AbstractException {
        Assert.assertEquals(project.getStatus(), Status.NOT_STARTED);
        projectEndpoint.startProjectById(session, projectId);
        Assert.assertEquals(projectEndpoint.findProjectById(session, projectId).getStatus(), Status.IN_PROGRESS);
    }

    @Test
    @Category(SoapCategory.class)
    public void completeProjectById() throws AbstractException {
        Assert.assertEquals(project.getStatus(), Status.NOT_STARTED);
        projectEndpoint.completeProjectById(session, projectId);
        Assert.assertEquals(projectEndpoint.findProjectById(session, projectId).getStatus(), Status.COMPLETE);
    }

    @Test
    @Category(SoapCategory.class)
    public void updateStatusByIdProject() throws AbstractException {
        Assert.assertEquals(project.getStatus(), Status.NOT_STARTED);
        projectEndpoint.changeProjectStatusById(session, projectId, Status.IN_PROGRESS);
        Assert.assertEquals(projectEndpoint.completeProjectById(session, projectId).getStatus(), Status.IN_PROGRESS);
        projectEndpoint.changeProjectStatusById(session, projectId, Status.COMPLETE);
        Assert.assertEquals(projectEndpoint.completeProjectById(session, projectId).getStatus(), Status.COMPLETE);
    }

    @After
    public void after() throws AbstractException {
        if (projectId != null)
            projectEndpoint.clearProjects(session);
        sessionEndpoint.closeSession(session);
    }

}
