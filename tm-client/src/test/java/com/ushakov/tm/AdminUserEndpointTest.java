package com.ushakov.tm;

import com.ushakov.tm.endpoint.AdminUserEndpoint;
import com.ushakov.tm.endpoint.AdminUserEndpointService;
import com.ushakov.tm.endpoint.SessionEndpoint;
import com.ushakov.tm.endpoint.SessionEndpointService;
import com.ushakov.tm.endpoint.Role;
import com.ushakov.tm.marker.SoapCategory;
import com.ushakov.tm.endpoint.Session;
import com.ushakov.tm.endpoint.User;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;

public class AdminUserEndpointTest {

    @NotNull
    private AdminUserEndpoint adminUserEndpoint;

    @NotNull
    private SessionEndpoint sessionEndpoint;

    @NotNull
    private final String userLogin = "temp";

    @NotNull
    private Session session;

    @Nullable
    private String userId;

    @NotNull
    private User user;

    @SneakyThrows
    public AdminUserEndpointTest() {

    }

    @Before
    @SneakyThrows
    public void before() {
        @NotNull final AdminUserEndpointService adminUserEndpointService = new AdminUserEndpointService();
        adminUserEndpoint = adminUserEndpointService.getAdminUserEndpointPort();
        @NotNull final SessionEndpointService sessionEndpointService = new SessionEndpointService();
        sessionEndpoint = sessionEndpointService.getSessionEndpointPort();
        session = sessionEndpoint.openSession("admin", "admin");
        user = adminUserEndpoint.createUser(session, userLogin, "temp");
        userId = user.getId();
    }

    @Test
    @SneakyThrows
    @Category(SoapCategory.class)
    public void createUser() {
        @NotNull final String tempUserLogin = "daemon";
        Assert.assertNotNull(adminUserEndpoint.createUser(session, tempUserLogin, "daemon"));
        Assert.assertNotNull(adminUserEndpoint.findUserByLogin(session, tempUserLogin));
        adminUserEndpoint.removeUserByLogin(session, tempUserLogin);
    }

    @Test
    @SneakyThrows
    @Category(SoapCategory.class)
    public void findAllUser() {
        Assert.assertFalse(adminUserEndpoint.findAllUsers(session).isEmpty());
    }

    @Test
    @SneakyThrows
    @Category(SoapCategory.class)
    public void findByIdUser() {
        @NotNull final User tempUser = adminUserEndpoint.findUserById(session, userId);
        Assert.assertNotNull(tempUser);
        Assert.assertEquals(userId, tempUser.getId());
    }

    @Test
    @SneakyThrows
    @Category(SoapCategory.class)
    public void findByLoginUser() {
        Assert.assertNotNull(adminUserEndpoint.findUserByLogin(session, userLogin));
    }

    @Test
    @SneakyThrows
    @Category(SoapCategory.class)
    public void removeByLoginUser() {
        Assert.assertTrue(userExists());
        adminUserEndpoint.removeUserByLogin(session, userLogin);
        userId = null;
        Assert.assertFalse(userExists());
    }

    @Test
    @SneakyThrows
    @Category(SoapCategory.class)
    public void removeByIdUser() {
        Assert.assertTrue(userExists());
        adminUserEndpoint.removeUserById(session, userId);
        userId = null;
        Assert.assertFalse(userExists());
    }

    @Test
    @SneakyThrows
    @Category(SoapCategory.class)
    public void setRoleUser() {
        Assert.assertEquals(user.getRole(), Role.USER);
        user.setRole(Role.ADMIN);
        Assert.assertEquals(adminUserEndpoint.findUserById(session, userId).getRole(), Role.ADMIN);
    }

    @Test
    @SneakyThrows
    @Category(SoapCategory.class)
    public void unlockLockByLoginUser() {
        Assert.assertFalse(user.isLocked());
        adminUserEndpoint.lockUserByLogin(session, userLogin);
        Assert.assertTrue(adminUserEndpoint.findUserById(session, userId).isLocked());
        adminUserEndpoint.unlockUserByLogin(session, userLogin);
        Assert.assertFalse(adminUserEndpoint.findUserById(session, userId).isLocked());
    }

    @After
    @SneakyThrows
    public void after() {
        if (userId != null) adminUserEndpoint.removeUserById(session, userId);
        sessionEndpoint.closeSession(session);
    }

    @SneakyThrows
    private boolean userExists() {
        return adminUserEndpoint
                .findAllUsers(session)
                .stream()
                .anyMatch(u -> u.getId().equals(userId));
    }



}
