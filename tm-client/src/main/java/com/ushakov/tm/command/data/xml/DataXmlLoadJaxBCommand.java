package com.ushakov.tm.command.data.xml;

import com.ushakov.tm.command.AbstractDataCommand;
import com.ushakov.tm.endpoint.Session;
import com.ushakov.tm.enumerated.Role;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public class DataXmlLoadJaxBCommand extends AbstractDataCommand {

    @Override
    @Nullable
    public String arg() {
        return null;
    }

    @Override
    @Nullable
    public String description() {
        return "Load data from xml file by JAXB.";
    }

    @SneakyThrows
    @Override
    public void execute() {
        @NotNull final Session session = endpointLocator.getSession();
        endpointLocator.getAdminDataEndpoint().loadDataXmlJaxB(session);
    }

    @Override
    @NotNull
    public String name() {
        return "data-load-xml-jaxb";
    }

    @Override
    @Nullable
    public Role[] roles() {
        return new Role[]{Role.ADMIN};
    }

}
