package com.ushakov.tm.command.system;

import com.ushakov.tm.command.AbstractCommand;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;

public class HelpCommand extends AbstractCommand {

    @Override
    @Nullable
    public String arg() {
        return "-h";
    }

    @Override
    @Nullable
    public String description() {
        return "Show terminal commands.";
    }

    @Override
    public void execute() {
        System.out.println("[HELP]");
        @NotNull final List<String> names = endpointLocator.getCommandService().getCommandNames();
        for (@NotNull final String name : names) {
            System.out.println(name);
        }
        System.out.println();
    }

    @Override
    @NotNull
    public String name() {
        return "help";
    }

}
