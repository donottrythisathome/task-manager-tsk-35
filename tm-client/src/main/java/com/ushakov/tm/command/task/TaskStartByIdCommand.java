package com.ushakov.tm.command.task;

import com.ushakov.tm.command.AbstractTaskCommand;
import com.ushakov.tm.endpoint.Session;
import com.ushakov.tm.endpoint.Task;
import com.ushakov.tm.enumerated.Role;
import com.ushakov.tm.util.TerminalUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public class TaskStartByIdCommand extends AbstractTaskCommand {

    @Override
    @Nullable
    public String arg() {
        return null;
    }

    @Override
    @Nullable
    public String description() {
        return "Start task by id.";
    }

    @Override
    public void execute() {
        @NotNull final Session session = endpointLocator.getSession();
        System.out.println("ENTER TASK ID");
        @NotNull final String taskId = TerminalUtil.nextLine();
        @NotNull final Task task = endpointLocator.getTaskEndpoint().startTaskById(session, taskId);
    }

    @Override
    @NotNull
    public String name() {
        return "start-task-by-id";
    }

    @Override
    @Nullable
    public Role[] roles() {
        return Role.values();
    }

}
