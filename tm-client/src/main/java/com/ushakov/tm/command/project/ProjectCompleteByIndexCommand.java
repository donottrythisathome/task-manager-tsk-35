package com.ushakov.tm.command.project;

import com.ushakov.tm.command.AbstractProjectCommand;
import com.ushakov.tm.endpoint.Project;
import com.ushakov.tm.endpoint.Session;
import com.ushakov.tm.enumerated.Role;
import com.ushakov.tm.util.TerminalUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public class ProjectCompleteByIndexCommand extends AbstractProjectCommand {

    @Override
    @Nullable
    public String arg() {
        return null;
    }

    @Override
    @Nullable
    public String description() {
        return "Complete project by index.";
    }

    @Override
    public void execute() {
        @NotNull final Session session = endpointLocator.getSession();
        System.out.println("ENTER PROJECT INDEX");
        @NotNull final Integer projectIndex = TerminalUtil.nextNumber();
        @NotNull final Project project = endpointLocator.getProjectEndpoint().completeProjectByIndex(session, projectIndex - 1);
    }

    @Override
    @NotNull
    public String name() {
        return "complete-project-by-index";
    }

    @Override
    @Nullable
    public Role[] roles() {
        return Role.values();
    }

}
