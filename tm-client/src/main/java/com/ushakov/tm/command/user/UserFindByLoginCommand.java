package com.ushakov.tm.command.user;

import com.ushakov.tm.command.AbstractUserCommand;
import com.ushakov.tm.endpoint.Session;
import com.ushakov.tm.endpoint.User;
import com.ushakov.tm.exception.entity.UserNotFoundException;
import com.ushakov.tm.util.TerminalUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Optional;

public class UserFindByLoginCommand extends AbstractUserCommand {

    @Override
    @Nullable
    public String arg() {
        return null;
    }

    @Override
    @Nullable
    public String description() {
        return "Find user by login.";
    }

    @Override
    public void execute() {
        System.out.println("ENTER USER LOGIN");
        @NotNull final Session session = endpointLocator.getSession();
        @NotNull final String login = TerminalUtil.nextLine();
        @Nullable final User user = endpointLocator.getAdminUserEndpoint().findUserByLogin(session, login);
        Optional.ofNullable(user).orElseThrow(UserNotFoundException::new);
        System.out.println(user);
    }

    @Override
    @NotNull
    public String name() {
        return "user-find-by-login";
    }

}
