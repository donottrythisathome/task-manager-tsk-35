package com.ushakov.tm.command.user;

import com.ushakov.tm.command.AbstractUserCommand;
import com.ushakov.tm.endpoint.Session;
import com.ushakov.tm.endpoint.User;
import com.ushakov.tm.exception.entity.ObjectNotFoundException;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;
import java.util.Optional;

public class UserShowListCommand extends AbstractUserCommand {

    @Override
    @Nullable
    public String arg() {
        return null;
    }

    @Override
    @Nullable
    public String description() {
        return "Show all users.";
    }

    @Override
    public void execute() {
        System.out.println("[USER LIST]");
        @NotNull final Session session = endpointLocator.getSession();
        @Nullable List<User> users = endpointLocator.getAdminUserEndpoint().findAllUsers(session);
        Optional.ofNullable(users).orElseThrow(ObjectNotFoundException::new);
        int index = 1;
        for (@NotNull final User user : users) {
            System.out.println(index + ". " + user);
            index++;
        }
    }

    @Override
    @NotNull
    public String name() {
        return "user-list";
    }

}
