package com.ushakov.tm.repository;

import com.ushakov.tm.api.repository.ICommandRepository;
import com.ushakov.tm.command.AbstractCommand;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.ArrayList;
import java.util.List;

public class CommandRepository implements ICommandRepository {

    @NotNull
    private final List<AbstractCommand> list = new ArrayList<>();

    @Override
    public void add(@NotNull final AbstractCommand command) {
        list.add(command);
    }

    @Override
    @Nullable
    public AbstractCommand getCommandByArg(@NotNull final String arg) {
        return list.stream()
                .filter(c -> arg.equals(c.arg()))
                .findFirst().orElse(null);
    }

    @Override
    @Nullable
    public AbstractCommand getCommandByName(@NotNull final String name) {
        return list.stream()
                .filter(c -> c.name().equals(name))
                .findFirst().orElse(null);
    }

    @Override
    @NotNull
    public List<String> getCommandNames() {
        @Nullable final List<String> names = new ArrayList<>();
        for (@NotNull final AbstractCommand command : list) {
            names.add(command.name());
        }
        return names;
    }

    @Override
    @NotNull
    public List<AbstractCommand> getCommands() {
        return list;
    }

    @Override
    @NotNull
    public List<AbstractCommand> getCommandsWithArguments() {
        @Nullable final List<AbstractCommand> commands = new ArrayList<>();
        for (@NotNull final AbstractCommand command : list) {
            if (command.arg() != null) commands.add(command);
        }
        return commands;
    }

}
