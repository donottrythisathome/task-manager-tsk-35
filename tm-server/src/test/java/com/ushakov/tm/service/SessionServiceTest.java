package com.ushakov.tm.service;

import com.ushakov.tm.api.repository.ISessionRepository;
import com.ushakov.tm.api.repository.IUserRepository;
import com.ushakov.tm.api.service.IPropertyService;
import com.ushakov.tm.api.service.ISessionService;
import com.ushakov.tm.api.service.IUserService;
import com.ushakov.tm.exception.AbstractException;
import com.ushakov.tm.model.Session;
import com.ushakov.tm.model.User;
import com.ushakov.tm.repository.SessionRepository;
import com.ushakov.tm.repository.UserRepository;
import com.ushakov.tm.util.HashUtil;
import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class SessionServiceTest {

    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final IUserRepository userRepository = new UserRepository();

    @NotNull
    private final ISessionRepository sessionRepository = new SessionRepository();

    @NotNull
    private final IUserService userService = new UserService(userRepository, propertyService);

    @NotNull
    private final ISessionService sessionService = new SessionService(userService, sessionRepository, propertyService);

    @NotNull
    private String userId;

    @NotNull
    private final String userLogin = "userLogin";

    @NotNull
    private final String userPassword = "userPassword";

    @NotNull
    private Session session;

    @Before
    public void before() throws AbstractException {
        @NotNull User user = new User();
        userId = user.getId();
        user.setLogin(userLogin);
        final int iteration = propertyService.getPasswordIteration();
        @NotNull final String secret = propertyService.getPasswordSecret();
        user.setPasswordHash(HashUtil.salt(secret, iteration, userPassword));
        userService.add(user);
        session = sessionService.open(userLogin, userPassword);
    }

    @Test
    public void open() {
        Assert.assertNotNull(session);
        Assert.assertEquals(userId, session.getUserId());
    }

    @Test
    public void close() {
        Assert.assertEquals(userId, session.getUserId());
        Assert.assertTrue(sessionService.close(session));
        Assert.assertFalse(sessionService.close(session));
    }

    @Test
    public void validate() throws AbstractException {
        sessionService.validate(session);
    }

    @After
    public void after() {
        sessionService.close(session);
    }

}
