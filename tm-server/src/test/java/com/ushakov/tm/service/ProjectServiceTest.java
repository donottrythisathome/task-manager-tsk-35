package com.ushakov.tm.service;

import com.ushakov.tm.api.repository.IProjectRepository;
import com.ushakov.tm.api.service.IProjectService;
import com.ushakov.tm.enumerated.Status;
import com.ushakov.tm.exception.AbstractException;
import com.ushakov.tm.model.Project;
import com.ushakov.tm.repository.ProjectRepository;
import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class ProjectServiceTest {

    @NotNull
    private IProjectService projectService;

    @NotNull
    private Project project;

    @NotNull
    private String projectId;

    @NotNull
    private final String projectName = "projectName";

    @NotNull
    private final String projectDescription = "projectDescription";

    @NotNull
    private final String userId = "userId";

    @Before
    public void before() throws AbstractException {
        project = new Project();
        projectId = project.getId();
        project.setName(projectName);
        project.setDescription(projectDescription);
        project.setUserId(userId);
        @NotNull final IProjectRepository projectRepository = new ProjectRepository();
        projectRepository.add(userId, project);
        projectService = new ProjectService(projectRepository);
    }

    @Test
    public void create() throws AbstractException {
        @NotNull final String newProjectName = "newProjectName";
        @NotNull final String newProjectDescription = "newProjectDescription";
        @NotNull final Project newProject = projectService.add(userId, newProjectName, newProjectDescription);
        Assert.assertEquals(2, projectService.findAll().size());
        Assert.assertEquals(newProject.getName(), newProjectName);
        Assert.assertEquals(newProject.getDescription(), newProjectDescription);
    }

    @Test
    public void findByName() throws AbstractException {
        Assert.assertEquals(project, projectService.findOneByName(userId, projectName));
        @NotNull final Project project = projectService.findOneByName(userId, projectName);
        Assert.assertEquals(project.getName(), projectName);
        Assert.assertEquals(project.getDescription(), projectDescription);
        Assert.assertEquals(project.getUserId(), userId);
    }

    @Test
    public void updateById() throws AbstractException {
        Assert.assertEquals(project, projectService.findOneByName(userId, projectName));
        @NotNull final String newProjectName = "newProjectName";
        @NotNull final String newProjectDescription = "newProjectDescription";
        projectService.updateById(userId, projectId, newProjectName, newProjectDescription);
        Assert.assertEquals(project, projectService.findOneById(userId, projectId));
        @NotNull final Project tempProject = projectService.findOneByName(userId, newProjectName);
        Assert.assertNotEquals(tempProject.getName(), projectName);
        Assert.assertNotEquals(tempProject.getDescription(), projectDescription);
        Assert.assertEquals(tempProject.getName(), newProjectName);
        Assert.assertEquals(tempProject.getDescription(), newProjectDescription);
        Assert.assertEquals(tempProject.getId(), projectId);
        Assert.assertEquals(tempProject.getUserId(), userId);
    }

    @Test
    public void updateByIndex() throws AbstractException {
        Assert.assertEquals(project, projectService.findOneByName(userId, projectName));
        @NotNull final String newProjectName = "newProjectName";
        @NotNull final String newProjectDescription = "newProjectDescription";
        projectService.updateByIndex(userId, 0, newProjectName, newProjectDescription);
        Assert.assertEquals(project, projectService.findOneByIndex(userId, 0));
        @NotNull final Project tempProject = projectService.findOneByName(userId, newProjectName);
        Assert.assertNotEquals(tempProject.getName(), projectName);
        Assert.assertNotEquals(tempProject.getDescription(), projectDescription);
        Assert.assertEquals(tempProject.getName(), newProjectName);
        Assert.assertEquals(tempProject.getDescription(), newProjectDescription);
        Assert.assertEquals(tempProject.getId(), projectId);
        Assert.assertEquals(tempProject.getUserId(), userId);
    }

    @Test
    public void startById() throws AbstractException {
        @NotNull final Project tempProject = projectService.findOneById(userId, projectId);
        Assert.assertNull(tempProject.getDateStart());
        Assert.assertEquals(tempProject.getStatus(), Status.NOT_STARTED);
        projectService.startById(userId, projectId);
        Assert.assertNotNull(tempProject.getDateStart());
        Assert.assertEquals(tempProject.getStatus(), Status.IN_PROGRESS);
    }

    @Test
    public void startByIndex() throws AbstractException {
        @NotNull final Project tempProject = projectService.findOneByIndex(userId, 0);
        Assert.assertNull(tempProject.getDateStart());
        Assert.assertEquals(tempProject.getStatus(), Status.NOT_STARTED);
        projectService.startByIndex(userId, 0);
        Assert.assertNotNull(tempProject.getDateStart());
        Assert.assertEquals(tempProject.getStatus(), Status.IN_PROGRESS);
    }

    @Test
    public void startByName() throws AbstractException {
        @NotNull final Project tempProject = projectService.findOneByName(userId, projectName);
        Assert.assertNull(tempProject.getDateStart());
        Assert.assertEquals(tempProject.getStatus(), Status.NOT_STARTED);
        projectService.startByName(userId, projectName);
        Assert.assertNotNull(tempProject.getDateStart());
        Assert.assertEquals(tempProject.getStatus(), Status.IN_PROGRESS);
    }

    @Test
    public void completeById() throws AbstractException {
        @NotNull final Project tempProject = projectService.findOneById(userId, projectId);
        Assert.assertEquals(tempProject.getStatus(), Status.NOT_STARTED);
        projectService.completeById(userId, projectId);
        Assert.assertEquals(tempProject.getStatus(), Status.COMPLETE);
    }

    @Test
    public void completeByIndex() throws AbstractException {
        @NotNull final Project tempProject = projectService.findOneByIndex(userId, 0);
        Assert.assertEquals(tempProject.getStatus(), Status.NOT_STARTED);
        projectService.completeByIndex(userId, 0);
        Assert.assertEquals(tempProject.getStatus(), Status.COMPLETE);
    }

    @Test
    public void completeByName() throws AbstractException {
        @NotNull final Project tempProject = projectService.findOneByName(userId, projectName);
        Assert.assertEquals(tempProject.getStatus(), Status.NOT_STARTED);
        projectService.completeByName(userId, projectName);
        Assert.assertEquals(tempProject.getStatus(), Status.COMPLETE);
    }

    @Test
    public void updateStatusById() throws AbstractException {
        @NotNull final Project tempProject = projectService.findOneById(userId, projectId);
        Assert.assertEquals(tempProject.getStatus(), Status.NOT_STARTED);
        projectService.changeStatusById(userId, projectId, Status.IN_PROGRESS);
        Assert.assertEquals(tempProject.getStatus(), Status.IN_PROGRESS);
        projectService.changeStatusById(userId, projectId, Status.COMPLETE);
        Assert.assertEquals(tempProject.getStatus(), Status.COMPLETE);
        projectService.changeStatusById(userId, projectId, Status.NOT_STARTED);
        Assert.assertEquals(tempProject.getStatus(), Status.NOT_STARTED);
    }

    @Test
    public void updateStatusByIndex() throws AbstractException {
        @NotNull final Project tempProject = projectService.findOneByIndex(userId, 0);
        Assert.assertEquals(tempProject.getStatus(), Status.NOT_STARTED);
        projectService.changeStatusByIndex(userId, 0, Status.IN_PROGRESS);
        Assert.assertEquals(tempProject.getStatus(), Status.IN_PROGRESS);
        projectService.changeStatusByIndex(userId, 0, Status.COMPLETE);
        Assert.assertEquals(tempProject.getStatus(), Status.COMPLETE);
        projectService.changeStatusByIndex(userId, 0, Status.NOT_STARTED);
        Assert.assertEquals(tempProject.getStatus(), Status.NOT_STARTED);
    }

    @Test
    public void updateStatusByName() throws AbstractException {
        @NotNull final Project tempProject = projectService.findOneByName(userId, projectName);
        Assert.assertEquals(tempProject.getStatus(), Status.NOT_STARTED);
        projectService.changeStatusByName(userId, projectName, Status.IN_PROGRESS);
        Assert.assertEquals(tempProject.getStatus(), Status.IN_PROGRESS);
        projectService.changeStatusByName(userId, projectName, Status.COMPLETE);
        Assert.assertEquals(tempProject.getStatus(), Status.COMPLETE);
        projectService.changeStatusByName(userId, projectName, Status.NOT_STARTED);
        Assert.assertEquals(tempProject.getStatus(), Status.NOT_STARTED);
    }

    @After
    public void after() throws AbstractException {
        projectService.clear();
    }

}
