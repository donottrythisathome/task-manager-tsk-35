package com.ushakov.tm.repository;

import com.ushakov.tm.exception.AbstractException;
import com.ushakov.tm.model.Project;
import com.ushakov.tm.model.Task;
import com.ushakov.tm.model.User;
import com.ushakov.tm.util.HashUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class TaskRepositoryTest {

    @Nullable
    private TaskRepository taskRepository;

    @Nullable
    private ProjectRepository projectRepository;

    @Nullable
    private Task task;

    @Nullable
    private Project project;

    @Nullable
    private User user;

    @Nullable
    private String userId;

    @NotNull
    private final String taskName = "newtask";

    @NotNull
    private final String projectName = "projectname";

    @Nullable
    private String taskId;

    @Nullable
    private String projectId;

    @Before
    public void before() {
        user = new User();
        user.setLogin("newuser");
        user.setPasswordHash(HashUtil.salt("newuserpass", 446, "werew"));
        taskRepository = new TaskRepository();
        task = new Task(taskName);
        taskId = task.getId();
        projectRepository = new ProjectRepository();
        project = new Project(projectName);
        projectId = project.getId();
    }

    @Test
    public void add() throws AbstractException {
        Assert.assertNotNull(taskRepository.add(userId, task));
    }

    @Test
    public void findTask() throws AbstractException {
        Assert.assertEquals(task, taskRepository.findOneByName(userId, taskName));
        Assert.assertEquals(task, taskRepository.findOneById(userId, taskId));
        Assert.assertEquals(task, taskRepository.findOneByIndex(userId,0));
    }

    @Test
    public void removeTask() throws AbstractException {
        Assert.assertNotNull(task);
        taskRepository.removeOneByName(userId, taskName);
        Assert.assertTrue(taskRepository.findAll().isEmpty());
    }

    @Test
    public void findAllByProjectId(
    ) throws AbstractException {
        task.setProjectId(projectId);
        Assert.assertEquals(task, taskRepository.findAllByProjectId(userId, projectId).get(0));
    }


    @After
    public void after() {
        taskRepository.clear(userId);
    }

}
