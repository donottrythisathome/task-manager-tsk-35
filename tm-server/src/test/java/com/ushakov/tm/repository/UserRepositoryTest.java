package com.ushakov.tm.repository;

import com.ushakov.tm.exception.AbstractException;
import com.ushakov.tm.model.User;
import com.ushakov.tm.util.HashUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class UserRepositoryTest {

    @Nullable
    private UserRepository userRepository;

    @Nullable
    private User user;

    @NotNull
    private String userLogin = "onemoreuser";

    @NotNull
    private String userEmail = "sample@mail.ru";

    @Before
    public void before() throws AbstractException {
        user = new User();
        user.setLogin(userLogin);
        user.setPasswordHash(HashUtil.salt("anotheruserpass", 446, "werew"));
        user.setEmail(userEmail);
        userRepository.add(user);
    }

    @Test
    public void findByLogin() throws AbstractException {
        Assert.assertEquals(user, userRepository.findOneByLogin(userLogin));
    }

    @Test
    public void findByEmail() throws AbstractException {
        Assert.assertEquals(user, userRepository.findOneByEmail(userEmail));
    }

    @Test
    public void removeByLogin() throws AbstractException {
        userRepository.removeOneByLogin(userLogin);
        Assert.assertTrue(userRepository.findAll().isEmpty());
    }

    @After
    public void after() {
        userRepository.clear();
    }

}
