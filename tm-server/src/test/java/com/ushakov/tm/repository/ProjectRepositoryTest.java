package com.ushakov.tm.repository;

import com.ushakov.tm.enumerated.Status;
import com.ushakov.tm.exception.AbstractException;
import com.ushakov.tm.model.Project;
import com.ushakov.tm.model.User;
import com.ushakov.tm.util.HashUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class ProjectRepositoryTest {

    @Nullable
    private ProjectRepository projectRepository;

    @Nullable
    private Project project;

    @Nullable
    private User user;

    @Nullable
    private String userId;

    @NotNull
    private final String projectName = "newproject";

    @Nullable
    private String projectId;

    @Before
    public void before() {
        user = new User();
        user.setLogin("newuser");
        user.setPasswordHash(HashUtil.salt("newuserpass", 446, "werew"));
        projectRepository = new ProjectRepository();
        project = new Project(projectName);
        projectId = project.getId();
    }

    @Test
    public void add() throws AbstractException {
        Assert.assertNotNull(projectRepository.add(userId, project));
    }

    @Test
    public void findProject() throws AbstractException {
        Assert.assertEquals(project, projectRepository.findOneByName(userId, projectName));
        Assert.assertEquals(project, projectRepository.findOneById(userId, projectId));
        Assert.assertEquals(project, projectRepository.findOneByIndex(userId,0));
    }

    @Test
    public void removeProject() throws AbstractException {
        Assert.assertNotNull(project);
        projectRepository.removeOneByName(userId, projectName);
        Assert.assertTrue(projectRepository.findAll().isEmpty());
    }

    @After
    public void after() {
        projectRepository.clear(userId);
    }

}
