package com.ushakov.tm.repository;

import com.ushakov.tm.api.repository.ISessionRepository;
import com.ushakov.tm.exception.AbstractException;
import com.ushakov.tm.model.Session;
import com.ushakov.tm.model.User;
import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class SessionRepositoryTest {

    @NotNull
    private ISessionRepository sessionRepository;

    @NotNull
    private Session session;

    @NotNull
    private String sessionId;

    @Before
    public void before() {
        sessionRepository = new SessionRepository();
        session = new Session();
        sessionId = session.getId();
        session.setUserId(new User().getId());
    }

    @Test
    public void openClose() throws AbstractException {
        sessionRepository.add(session);
        Assert.assertEquals(session, sessionRepository.findOneById(sessionId));
        sessionRepository.remove(session);
        Assert.assertTrue(sessionRepository.findAll().isEmpty());
    }

    @Test
    public void contains() {
        sessionRepository.add(session);
        Assert.assertTrue(sessionRepository.contains(sessionId));
    }

    @After
    public void after() {
        sessionRepository.clear();
    }

}
