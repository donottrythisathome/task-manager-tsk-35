package com.ushakov.tm.repository;

import com.ushakov.tm.api.repository.IUserRepository;
import com.ushakov.tm.exception.entity.UserNotFoundException;
import com.ushakov.tm.model.User;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Optional;

public class UserRepository extends AbstractRepository<User> implements IUserRepository {

    @Override
    @Nullable
    public User findOneByEmail(@NotNull final String email) {
        return list.stream()
                .filter(u -> u.getEmail().equals(email))
                .findFirst().orElse(null);
    }

    @Override
    @Nullable
    public User findOneByLogin(@NotNull final String login) {
        return list.stream()
                .filter(u -> u.getLogin().equals(login))
                .findFirst().orElse(null);
    }

    @Override
    @Nullable
    public User removeOneByLogin(@NotNull final String login) {
        @Nullable final User user = findOneByLogin(login);
        Optional.ofNullable(user).orElseThrow(UserNotFoundException::new);
        remove(user);
        return user;
    }

}
