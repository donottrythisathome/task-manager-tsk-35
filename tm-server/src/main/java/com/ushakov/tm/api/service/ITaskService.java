package com.ushakov.tm.api.service;

import com.ushakov.tm.model.Task;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public interface ITaskService extends IOwnerBusinessService<Task> {

    @NotNull
    Task add(@Nullable String userId, @Nullable String name, @Nullable String description);

}