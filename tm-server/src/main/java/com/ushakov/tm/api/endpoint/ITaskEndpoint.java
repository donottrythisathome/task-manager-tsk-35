package com.ushakov.tm.api.endpoint;

import com.ushakov.tm.enumerated.Status;
import com.ushakov.tm.model.Session;
import com.ushakov.tm.model.Task;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import java.util.List;

public interface ITaskEndpoint {

    @WebMethod
    void addAllTasks(
            @Nullable @WebParam(name = "session") Session session,
            @NotNull @WebParam(name = "entities") List<Task> entities
    );

    @WebMethod
    void addTask(
            @Nullable @WebParam(name = "session") Session session,
            @NotNull @WebParam(name = "entity") Task entity
    );

    @WebMethod
    @NotNull Task changeTaskStatusById(
            @Nullable @WebParam(name = "session") Session session,
            @Nullable @WebParam(name = "id") String id,
            @Nullable @WebParam(name = "status") Status status
    );

    @WebMethod
    @NotNull Task changeTaskStatusByIndex(
            @Nullable @WebParam(name = "session") Session session,
            @Nullable @WebParam(name = "index") Integer index,
            @Nullable @WebParam(name = "status") Status status
    );

    @WebMethod
    @NotNull Task changeTaskStatusByName(
            @Nullable @WebParam(name = "session") Session session,
            @Nullable @WebParam(name = "name") String name,
            @Nullable @WebParam(name = "status") Status status
    );

    @WebMethod
    void clearTasks(@Nullable @WebParam(name = "session") Session session);

    @WebMethod
    @NotNull Task completeTaskById(
            @Nullable @WebParam(name = "session") Session session,
            @Nullable @WebParam(name = "id") String id
    );

    @WebMethod
    @NotNull Task completeTaskByIndex(
            @Nullable @WebParam(name = "session") Session session,
            @Nullable @WebParam(name = "index") Integer index
    );

    @WebMethod
    @NotNull Task completeTaskByName(
            @Nullable @WebParam(name = "session") Session session,
            @Nullable @WebParam(name = "name") String name
    );

    @WebMethod
    void createTask(
            @Nullable @WebParam(name = "session") Session session,
            @Nullable @WebParam(name = "name") String name,
            @Nullable @WebParam(name = "description") String description
    );

    @WebMethod
    @NotNull List<Task> findAllTasks(@Nullable @WebParam(name = "session") Session session);

    @WebMethod
    @NotNull Task findTaskById(
            @Nullable @WebParam(name = "session") Session session,
            @NotNull @WebParam(name = "id") String id
    );

    @WebMethod
    @Nullable Task findTaskByIndex(
            @Nullable @WebParam(name = "session") Session session,
            @NotNull @WebParam(name = "index") Integer index
    );

    @WebMethod
    @NotNull Task findTaskByName(
            @Nullable @WebParam(name = "session") Session session,
            @Nullable @WebParam(name = "name") String name
    );

    @WebMethod
    @Nullable Task removeTaskById(
            @Nullable @WebParam(name = "session") Session session,
            @NotNull @WebParam(name = "id") String id
    );

    @WebMethod
    @Nullable Task removeTaskByIndex(
            @Nullable @WebParam(name = "session") Session session,
            @NotNull @WebParam(name = "index") Integer index
    );

    @WebMethod
    @Nullable
    Task removeTaskByName(
            @Nullable @WebParam(name = "session") Session session,
            @Nullable @WebParam(name = "name") String name
    );

    @WebMethod
    @NotNull Task startTaskById(
            @Nullable @WebParam(name = "session") Session session,
            @Nullable @WebParam(name = "id") String id
    );

    @WebMethod
    @NotNull Task startTaskByIndex(
            @Nullable @WebParam(name = "session") Session session,
            @Nullable @WebParam(name = "index") Integer index
    );

    @WebMethod
    @NotNull Task startTaskByName(
            @Nullable @WebParam(name = "session") Session session,
            @Nullable @WebParam(name = "name") String name
    );

    @WebMethod
    @Nullable Task unbindTaskFromProject(
            @Nullable @WebParam(name = "session") final Session session,
            @Nullable @WebParam(name = "taskId") final String taskId
    );

    @WebMethod
    void updateTaskById(
            @Nullable @WebParam(name = "session") Session session,
            @Nullable @WebParam(name = "id") String id,
            @Nullable @WebParam(name = "name") String name,
            @Nullable @WebParam(name = "description") String description
    );

    @WebMethod
    void updateTaskByIndex(
            @Nullable @WebParam(name = "session") Session session,
            @Nullable @WebParam(name = "index") Integer index,
            @Nullable @WebParam(name = "name") String name,
            @Nullable @WebParam(name = "description") String description
    );

}
