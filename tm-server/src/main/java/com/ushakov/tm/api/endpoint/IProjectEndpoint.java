package com.ushakov.tm.api.endpoint;

import com.ushakov.tm.enumerated.Status;
import com.ushakov.tm.model.Project;
import com.ushakov.tm.model.Session;
import com.ushakov.tm.model.Task;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import java.util.List;

public interface IProjectEndpoint {

    @WebMethod
    void addAllProjects(
            @Nullable @WebParam(name = "session") Session session,
            @Nullable @WebParam(name = "entities", partName = "entities") List<Project> entities
    );

    @WebMethod
    void addProject(@Nullable @WebParam(name = "session") Session session,
                    @Nullable @WebParam(name = "entity") Project entity
    );

    @WebMethod
    @Nullable Task bindTaskToProject(
            @Nullable @WebParam(name = "session") final Session session,
            @Nullable @WebParam(name = "taskId") final String taskId,
            @Nullable @WebParam(name = "projectId") final String projectId
    );

    @WebMethod
    @NotNull Project changeProjectStatusById(
            @Nullable @WebParam(name = "session") Session session,
            @Nullable @WebParam(name = "id") String id,
            @Nullable @WebParam(name = "status") Status status
    );

    @WebMethod
    @NotNull Project changeProjectStatusByIndex(
            @Nullable @WebParam(name = "session") Session session,
            @Nullable @WebParam(name = "index") Integer index,
            @Nullable @WebParam(name = "status") Status status
    );

    @WebMethod
    @NotNull Project changeProjectStatusByName(
            @Nullable @WebParam(name = "session") Session session,
            @Nullable @WebParam(name = "name") String name,
            @Nullable @WebParam(name = "status") Status status
    );

    @WebMethod
    void clearProjects(@Nullable @WebParam(name = "session") Session session);

    @WebMethod
    @NotNull Project completeProjectById(
            @Nullable @WebParam(name = "session") Session session,
            @Nullable @WebParam(name = "id") String id
    );

    @WebMethod
    @NotNull Project completeProjectByIndex(
            @Nullable @WebParam(name = "session") Session session,
            @Nullable @WebParam(name = "index") Integer index
    );

    @WebMethod
    @NotNull Project completeProjectByName(
            @Nullable @WebParam(name = "session") Session session,
            @Nullable @WebParam(name = "name") String name
    );

    @WebMethod
    void createProject(
            @Nullable @WebParam(name = "session") Session session,
            @Nullable @WebParam(name = "name") String name,
            @Nullable @WebParam(name = "description") String description
    );

    @WebMethod
    @Nullable
    List<Project> findAllProjects(@Nullable @WebParam(name = "session") Session session);


    @WebMethod
    @NotNull Project findProjectById(
            @Nullable @WebParam(name = "session") Session session,
            @Nullable @WebParam(name = "id") String id
    );


    @WebMethod
    @NotNull Project findProjectByIndex(
            @Nullable @WebParam(name = "session") Session session,
            @NotNull @WebParam(name = "index") Integer index
    );

    @WebMethod
    @NotNull Project findProjectByName(
            @Nullable @WebParam(name = "session") Session session,
            @Nullable @WebParam(name = "name") String name
    );

    @WebMethod
    @Nullable Project removeProjectById(
            @Nullable @WebParam(name = "session") Session session,
            @NotNull @WebParam(name = "id") String id
    );

    @WebMethod
    @Nullable Project removeProjectByIndex(
            @Nullable @WebParam(name = "session") Session session,
            @Nullable @WebParam(name = "index") Integer index
    );

    @WebMethod
    @Nullable Project removeProjectByName(
            @Nullable @WebParam(name = "session") Session session,
            @NotNull @WebParam(name = "name") String name
    );

    @WebMethod
    @NotNull Project startProjectById(
            @Nullable @WebParam(name = "session") Session session,
            @Nullable @WebParam(name = "id") String id
    );

    @WebMethod
    @NotNull Project startProjectByIndex(
            @Nullable @WebParam(name = "session") Session session,
            @Nullable @WebParam(name = "index") Integer index
    );

    @WebMethod
    @NotNull Project startProjectByName(
            @Nullable @WebParam(name = "session") Session session,
            @Nullable @WebParam(name = "name") String name
    );

    @WebMethod
    void updateProjectById(
            @Nullable @WebParam(name = "session") Session session,
            @Nullable @WebParam(name = "id") String id,
            @Nullable @WebParam(name = "name") String name,
            @NotNull @WebParam(name = "description") String description);

    @WebMethod
    void updateProjectByIndex(
            @Nullable @WebParam(name = "session") Session session,
            @Nullable @WebParam(name = "index") Integer index,
            @Nullable @WebParam(name = "name") String name,
            @NotNull @WebParam(name = "description") String description
    );

}
